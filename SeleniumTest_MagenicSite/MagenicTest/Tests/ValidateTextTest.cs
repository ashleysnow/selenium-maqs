﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageModel;


// TODO: Add reference to object model
// using PageModel;

namespace Tests
{
    /// <summary>
    /// ValidateTextTest test class
    /// </summary>
    [TestClass]
    public class ValidateTextTest : BaseSeleniumTest
    {
        /// <summary>
        /// Sample test
        /// </summary>
        [TestMethod]
        public void TestText()
        {
            // TODO: Add test code
            //PageModel page = new PageModel(this.WebDriver);
            //page.OpenPage();
            Homepage homepage = new Homepage(this.WebDriver);
            homepage.OpenPage();
            homepage.clickIndustriesLink();

            IndustriesPage industriespage = new IndustriesPage(this.WebDriver);
            industriespage.clickThinkingLink();

            ThinkingPage technologiesPage = new ThinkingPage(this.WebDriver);
            Assert.AreEqual("The thinking behind the work", technologiesPage.ValidateText(), true);

        }
    }
}
