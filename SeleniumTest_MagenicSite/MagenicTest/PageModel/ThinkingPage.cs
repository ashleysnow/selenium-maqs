﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for TechnologiesPage
    /// </summary>
    public class ThinkingPage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "https://magenic.com/thinking";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        private static By modernAppText = By.CssSelector("body > section.hero.blog > div > div > h1");

        public string ValidateText()
        {
            return webDriver.WaitForClickableElement(modernAppText).Text;
        }
        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThinkingPage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public ThinkingPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
    }
}
