﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for Model1
    /// </summary>
    public class Homepage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "https://magenic.com/";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        private static By industriesLink = By.CssSelector("body > header > nav > ul > li:nth-child(1) > a");

        public void clickIndustriesLink()
        {
            webDriver.WaitForClickableElement(industriesLink).Click();
    }
     
    

    /// <summary>
    /// Selenium Web Driver
    /// </summary>
    private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="Homepage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public Homepage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
    }
}
