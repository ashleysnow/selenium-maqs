﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for Model1
    /// </summary>
    public class IndustriesPage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "https://magenic.com/industries";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        private static By thinkingLink = By.CssSelector("body > header > nav > ul > li:nth-child(2) > a");


        public void clickThinkingLink()
        {
            webDriver.WaitForClickableElement(thinkingLink).Click();
        }
        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndustriesPage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public IndustriesPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
    }
}
