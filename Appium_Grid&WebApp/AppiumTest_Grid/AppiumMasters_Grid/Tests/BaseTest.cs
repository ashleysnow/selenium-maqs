﻿using System;
using Magenic.MaqsFramework.BaseAppiumTest;
using Magenic.MaqsFramework.Utilities.Data;
using Magenic.MaqsFramework.Utilities.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;

namespace Tests
{
    /// <summary>
    /// AppiumTest Base test class
    /// </summary>
    [TestClass]
    public class BaseTest : BaseAppiumTest
    {
        private static object obj = new object();

        /// <summary>
        /// The default get appium driver function
        /// </summary>
        /// <returns>The appium driver</returns>
        protected override AppiumDriver<AppiumWebElement> GetMobileDevice()
        {
            lock (obj)
            {
                AndroidDriver<AppiumWebElement>  appiumDriver = new AndroidDriver<AppiumWebElement>(new Uri(Config.GetValue("MobileHubUrl")), GetMobileCapabilitiesGrid());
                return appiumDriver;
            }
        }

        /// <summary>
        /// Get the mobile desired capability
        /// </summary>
        /// <returns>The mobile desired capability</returns>
        private DesiredCapabilities GetMobileCapabilitiesGrid()
        {
            DesiredCapabilities capabilities;
            string mobileDeivceOS = AppiumConfig.GetMobileDeviceOS();

            switch (mobileDeivceOS.ToUpper())
            {
                case "ANDROID":
                    capabilities = DesiredCapabilities.Android();
                    break;

                case "IOS":
                    capabilities = DesiredCapabilities.IPhone();
                    break;

                default:
                    throw new Exception(StringProcessor.SafeFormatter("Remote browser type '{0}' is not supported", mobileDeivceOS));
            }

         
            capabilities.SetCapability("deviceName", AppiumConfig.GetDeviceName());
            capabilities.SetCapability("app", Config.GetValue("AdroidAppPath"));
            capabilities.SetCapability("appActivity", Config.GetValue("AdroidAppActivity"));

            return capabilities;
        }
    }
}
