﻿using Magenic.MaqsFramework.BaseAppiumTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageModel;

namespace Tests
{
    /// <summary>
    /// AppiumTest test class
    /// </summary>
    [TestClass]
    public class AppiumTest : BaseTest
    {
        /// <summary>
        /// Enter Credentials test
        /// </summary>
        [TestMethod]
        public void EnterCredentials()
        {

            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingLogin", "TestLogin@email.com");

            var actualTitle = homepage.GetActivityTitle();
            Assert.IsTrue(actualTitle.Equals(Home.Title), $"Activity Title does not match. Expected = {Home.Title} Actual = {actualTitle}");


        }
        /// <summary>
        /// Enter Credentials test
        /// </summary>
      /*  [TestMethod]
        public void EnterCredentialsWorking2()
        {
            LoginPageModel loginPage = new LoginPageModel(this.AppiumDriver);
            Home homePage = loginPage.EnterCredentials("TestUserName", "TestEmail@email.com");

            var actualTitle = homePage.GetActivityTitle();
            Assert.IsTrue(actualTitle.Equals(HomePageModel.Title), $"Activity Title does not match. Expected = {HomePageModel.Title}, Actual = {actualTitle}");
        }*/

        /// <summary>
        /// Enter Credentials test
        /// </summary>
        [TestMethod]
        public void MathTest()
        {
            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingMath", "TestingMath@email.com");
            FlyOut flyOutModel = homepage.clickHamburger();
            Calculator calculator = flyOutModel.ClickMath();
            calculator.EnterNumbers("5", "4");
          
        }

        /// <summary>
        /// Enter Credentials test
        /// </summary>
        [TestMethod]
        public void StoryTestCode()
        {
            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingStory", "TestStory@email.com");
            FlyOut flyOutModel = homepage.clickHamburger();
            StoryOne pageOne = flyOutModel.ClickStory();
            StoryTwo pageTwo = pageOne.ClickButtonOne();
            StoryThree pageThree = pageTwo.ClickButtonTwo();
            StoryFour pageFour = pageThree.ClickButtonThree();
            StoryOne pageone = pageFour.ClickFinalButton();

        }
    }
}
