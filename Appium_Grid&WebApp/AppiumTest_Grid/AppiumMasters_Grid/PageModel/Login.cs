﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    /// <summary>
    /// Page object for the LoginPageModel
    /// </summary>
    public class Login
    {
        /// <summary>
        /// The user name input element 'By' finder
        /// </summary>
        private static By userNameInput = By.XPath("//android.widget.EditText[contains(@resource-id, 'usernameField')]");

        /// <summary>
        /// The password input element 'By' finder
        /// </summary>
        private static By passwordInput = By.XPath("//android.widget.EditText[contains(@resource-id, 'emailField')]");

        /// <summary>
        /// The login button element 'By' finder
        /// </summary>
        private static By loginButton = By.XPath("//android.widget.Button[contains(@resource-id, 'loginButton')]");

        /// <summary>
        /// The login error message element 'By' finder
        /// </summary>
        private static By loginErrorMessage = By.XPath("//*/UIAStaticText[@label='ErrorMessage']");

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="Login" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public Login(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        /// <summary>
        /// Enter the use credentials
        /// </summary>
        /// <param name="userName">The user name</param>
        /// <param name="password">The user password</param>
        public Home EnterCredentials(string userName, string password)
        {
            this.appiumDriver.WaitForVisibleElement(userNameInput).SendKeys(userName);
            this.appiumDriver.WaitForVisibleElement(passwordInput).SendKeys(password);
            this.appiumDriver.WaitForVisibleElement(loginButton).Click();
            return new Home(this.appiumDriver);
        }
    }
}