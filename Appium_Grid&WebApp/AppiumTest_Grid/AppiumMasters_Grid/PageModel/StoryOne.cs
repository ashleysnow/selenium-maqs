﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class StoryOne
    {
        private static By ButtonOne = By.Id("com.example.tonyba.magenictestapp:id/button3");

        private AppiumDriver<AppiumWebElement> appiumDriver;

        public StoryOne(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }


        public StoryTwo ClickButtonOne()
        {
            this.appiumDriver.WaitForClickableElement(ButtonOne).Click();
            return new StoryTwo(this.appiumDriver);

        }

    }
}
