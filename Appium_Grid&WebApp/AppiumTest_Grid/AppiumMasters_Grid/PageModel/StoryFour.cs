﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class StoryFour
    {

        private static By ButtonFour = By.Id("com.example.tonyba.magenictestapp:id/button4");

        private AppiumDriver<AppiumWebElement> appiumDriver;

        public StoryFour(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public StoryOne ClickFinalButton()
        {
            this.appiumDriver.WaitForClickableElement(ButtonFour).Click();
            return new StoryOne(this.appiumDriver);

        }
    }
}
