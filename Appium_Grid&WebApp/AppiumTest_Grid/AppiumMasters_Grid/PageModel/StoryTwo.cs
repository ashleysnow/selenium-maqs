﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class StoryTwo
    {

        private static By ButtonTwo = By.Id("com.example.tonyba.magenictestapp:id/button4");


        private AppiumDriver<AppiumWebElement> appiumDriver;

        public StoryTwo(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public StoryThree ClickButtonTwo()
        {
            this.appiumDriver.WaitForClickableElement(ButtonTwo).Click();
            return new StoryThree(this.appiumDriver);

        }
    }
}
