﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class StoryThree
    {

        private static By ButtonThree = By.Id("com.example.tonyba.magenictestapp:id/button4");


        private AppiumDriver<AppiumWebElement> appiumDriver;

        public StoryThree(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public StoryFour ClickButtonThree()
        {
            this.appiumDriver.WaitForClickableElement(ButtonThree).Click();
            return new StoryFour(this.appiumDriver);
        }

    }
}
