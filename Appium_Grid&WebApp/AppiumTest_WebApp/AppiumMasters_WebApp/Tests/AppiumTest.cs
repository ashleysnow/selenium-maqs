﻿using Magenic.MaqsFramework.BaseAppiumTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageModel;

namespace Tests
{
    /// <summary>
    /// AppiumTest test class
    /// </summary>
    [TestClass]
    public class AppiumTest : BaseAppiumTest
    {
        /// <summary>
        /// Application Install test
        /// </summary>
        [TestMethod]
        public void SearchForMagenic()
        {
            var lp = new GooglePageModel(this.AppiumDriver);
            lp.NavigateToGoogle();
            var magenicHp = lp.SearchForMagenic("Magenic");
            var titleText = magenicHp.GetUrl();
            Assert.IsTrue(titleText.Equals("https://magenic.com/"), $"Url Did not match 'https://magenic.com/'. Actual = '{titleText}'");
            magenicHp.ClickMenu();
        }

        [TestMethod]
        public void NavigateToIndustries()
        {
            var lp = new GooglePageModel(this.AppiumDriver);
            lp.NavigateToGoogle();
            var magenicHp = lp.SearchForMagenic("Magenic");
            var titleText = magenicHp.GetUrl();
            magenicHp.ClickMenu();
            Menu menu = new Menu(this.AppiumDriver);
            Industries industries = menu.ClickIndustries();
            industries.identifyIndustriesText();
            menu = industries.industriesRetrunsToMenu();
            WhatWeDo whatWeDo = menu.ClickWhatWeDo();
            whatWeDo.identifyWhatWeDoText();
            menu = whatWeDo.WhatWeDoReturnToMenu();
            Thinking thinking = menu.ClickThinking();
            thinking.identifyThinkingText();

        }
    }
}
