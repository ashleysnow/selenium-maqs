﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class Thinking
    {

        private static By thinkingText = By.XPath("/html/body/section[1]/div/div/h1");
        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="MagenicHomePageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public Thinking(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public string identifyThinkingText()
        {
            return this.appiumDriver.FindElement(thinkingText).Text;
        }

    }
}
