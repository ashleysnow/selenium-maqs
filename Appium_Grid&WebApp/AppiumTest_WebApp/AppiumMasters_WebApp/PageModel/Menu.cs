﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class Menu
    {

        private static By industriesLink = By.XPath("/html/body/div[1]/nav/ul/li[1]/div/a");

        private static By whatWeDoLink = By.XPath("/html/body/div[1]/nav/ul/li[2]/div/a");

        private static By thinkingLink = By.XPath("/html/body/div[1]/nav/ul/li[3]/a");

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public Menu(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }


        public Industries ClickIndustries()
        {
            this.appiumDriver.WaitForClickableElement(industriesLink).Click();
            return new Industries(this.appiumDriver);
        }

        public WhatWeDo ClickWhatWeDo()
        {
            this.appiumDriver.WaitForClickableElement(whatWeDoLink).Click();
            return new WhatWeDo(this.appiumDriver);

        }

        public Thinking ClickThinking()
        {
            this.appiumDriver.WaitForClickableElement(thinkingLink).Click();
            return new Thinking(this.appiumDriver);
        }

    }
}
