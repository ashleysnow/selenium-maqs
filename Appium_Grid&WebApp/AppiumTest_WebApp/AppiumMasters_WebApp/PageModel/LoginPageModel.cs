﻿using System.Threading;
using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    /// <summary>
    /// Page object for the LoginPageModel
    /// </summary>
    public class GooglePageModel
    {
        /// <summary>
        /// The user name input element 'By' finder
        /// </summary>
        private static By searchInput = By.XPath("//input[contains(@name,'q')]");

        private static By searchButton = By.XPath("//button[contains(@id,'tsbb')]");

        private static By magenicLink = By.XPath("//a[@href = 'https://magenic.com/']");

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public GooglePageModel(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public void NavigateToGoogle()
        {
            this.appiumDriver.Navigate().GoToUrl("http://www.google.com");
            this.appiumDriver.WaitForPageLoad();
        }

        /// <summary>
        /// Enters the text into the search bar
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public MagenicHomePageModel SearchForMagenic(string search)
        {
            this.appiumDriver.WaitForVisibleElement(searchInput).SendKeys(search);
            this.appiumDriver.FindElement(searchButton).Click();
            this.appiumDriver.WaitForVisibleElement(magenicLink).Click();
            this.appiumDriver.WaitForPageLoad();
            return new MagenicHomePageModel(this.appiumDriver);
        }
    }
}