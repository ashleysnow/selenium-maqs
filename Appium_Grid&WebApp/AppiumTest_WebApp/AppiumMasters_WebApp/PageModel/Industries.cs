﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class Industries
    {
        private static By industriesText = By.XPath("/html/body/section[1]/div/div/h1");
     
        private static By menuButton = By.XPath("/html/body/header/div");

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="MagenicHomePageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public Industries(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public string identifyIndustriesText()
        {
            return this.appiumDriver.FindElement(industriesText).Text;
        }

        public Menu industriesRetrunsToMenu()
        {
            this.appiumDriver.WaitForClickableElement(menuButton).Click();
            return new Menu(this.appiumDriver);

        }



    }
}
