﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class WhatWeDo
    {

        private static By whatWeDoText = By.XPath("/html/body/section[1]/div/div/h1");

        private static By menuButton = By.XPath("/html/body/header/div");
        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="MagenicHomePageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public WhatWeDo(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public string identifyWhatWeDoText()
        {
            return this.appiumDriver.FindElement(whatWeDoText).Text;
        }

        public Menu WhatWeDoReturnToMenu()
        {
            this.appiumDriver.WaitForClickableElement(menuButton).Click();
            return new Menu(this.appiumDriver);

        }

    }
}
