﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    /// <summary>
    /// Page object for HomePageModel
    /// </summary>
    public class MagenicHomePageModel
    {
        /// <summary>
        /// The user name input element 'By' finder
        /// </summary>
        private static By menuButton = By.XPath("//body[@class = 'home']/header/div[@class = 'menu']");

        /// <summary>
        /// The user name input element 'By' finder
        /// </summary>
        private static string url = "https://magenic.com";

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="MagenicHomePageModel" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public MagenicHomePageModel(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        /// <summary>
        /// Get username text from label
        /// </summary>
        /// <returns>username text string</returns>
        public string GetUrl()
        {
            return this.appiumDriver.Url;
        }

        /// <summary>
        /// Clicks the thinking link
        /// </summary>
        /// <returns>username text string</returns>
        public void ClickMenu()
        {
            this.appiumDriver.FindElement(menuButton).Click();
            this.appiumDriver.WaitForPageLoad();
        }
    }
}