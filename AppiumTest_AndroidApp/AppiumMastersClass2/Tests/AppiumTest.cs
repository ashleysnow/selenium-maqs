﻿using Magenic.MaqsFramework.BaseAppiumTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageModel;

namespace Tests
{
    /// <summary>
    /// AppiumTest test class
    /// </summary>
    [TestClass]
    public class AppiumTest : BaseAppiumTest
    {
        /// <summary>
        /// Application Install test
        /// </summary>
    /*    [TestMethod]
        public void LaunchApplicationTest()
        {
            bool isInstalled = this.AppiumDriver.IsAppInstalled(AppiumConfig.GetBundleId());
        }
        */
        /// <summary>
        /// Enter credentials test
        /// </summary>
        [TestMethod]
        public void EnterCredentials()
        {

            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingLogin", "TestLogin@email.com");

            var actualTitle = homepage.GetActivityTitle();
            Assert.IsTrue(actualTitle.Equals(Home.Title), $"Activity Title does not match. Expected = {Home.Title} Actual = {actualTitle}");


        }

        [TestMethod]
        public void StoryWalkthrough()
        {
            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingStory", "TestStory@email.com");
            FlyOut flyOutModel = homepage.clickHamburger();
            StoryOne pageOne = flyOutModel.ClickStory();
            StoryTwo pageTwo = pageOne.ClickButtonOne();
            StoryThree pageThree = pageTwo.ClickButtonTwo();
            StoryFour pageFour = pageThree.ClickButtonThree();
            StoryOne pageone = pageFour.ClickFinalButton();
        }

        [TestMethod]
        public void MathCalculation()
        {
            Login loginPage = new Login(this.AppiumDriver);
            Home homepage = loginPage.EnterCredentials("TestingMath", "TestingMath@email.com");
            FlyOut flyOutModel = homepage.clickHamburger();
            Calculator calculator = flyOutModel.ClickMath();
            calculator.EnterNumbers("5","4");


        }


    }
}
