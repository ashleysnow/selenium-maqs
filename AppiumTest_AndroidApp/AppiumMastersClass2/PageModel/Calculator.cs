﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class Calculator
    {

        private static By firstNumberInput = By.XPath("//android.widget.EditText[contains(@resource-id, 'add1')]");
        private static By secondNumberInput = By.XPath("//android.widget.EditText[contains(@resource-id, 'add2')]");
        private static By CalculateButton = By.XPath("//android.widget.Button[contains(@resource-id, 'btnAdd')]");


        private AppiumDriver<AppiumWebElement> appiumDriver;

        public Calculator(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public void EnterNumbers(string firstNumber, string secondNumber)
        {

            this.appiumDriver.WaitForClickableElement(firstNumberInput).SendKeys(firstNumber);
            this.appiumDriver.WaitForClickableElement(secondNumberInput).SendKeys(secondNumber);
            this.appiumDriver.WaitForClickableElement(CalculateButton).Click();

        }

    }
}
