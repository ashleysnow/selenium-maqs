﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    /// <summary>
    /// Page object for HomePageModel
    /// </summary>
    public class Home
    {

        public static string Title = "App Central";

        /// <summary>
        /// The hamburger menu input element 'By' finder
        /// </summary>
        private static By ActivityTitle = By.XPath("//android.view.ViewGroup[contains(@resource-id, 'toolbar')]/android.widget.TextView");

        private static By FlyOutButton = By.XPath("//android.widget.ImageButton[@content-desc='Open navigation drawer']");

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private AppiumDriver<AppiumWebElement> appiumDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="Home" /> class.
        /// </summary>
        /// <param name="appiumDriver">The selenium web driver</param>
        public Home(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        /// <summary>
        /// Get the Activity Title
        /// </summary>
        /// <returns>Activity Title</returns>
        public string GetActivityTitle()
        {
            return this.appiumDriver.FindElement(ActivityTitle).Text;
        }

        public FlyOut clickHamburger()
        {
            this.appiumDriver.WaitForVisibleElement(FlyOutButton).Click();

            return new FlyOut(this.appiumDriver);        

        }

    }
    }
