﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;

namespace PageModel
{
    public class FlyOut
    {
     //   private static By HomeLink = By.XPath("//android.widget.CheckedTextView[contains(@resource-id, 'Home')]");                               

        private static By MathLink = By.XPath("//android.widget.CheckedTextView[@text = 'Math']");

        private static By StoryLink = By.XPath("//android.widget.CheckedTextView[@text = 'Story']");
                                          
        private AppiumDriver<AppiumWebElement> appiumDriver;

        public FlyOut(AppiumDriver<AppiumWebElement> appiumDriver)
        {
            this.appiumDriver = appiumDriver;
        }

        public StoryOne ClickStory()
        {
            this.appiumDriver.WaitForVisibleElement(StoryLink).Click();
            return new StoryOne(this.appiumDriver);
        }

        public Calculator ClickMath()
        {
            this.appiumDriver.WaitForVisibleElement(MathLink).Click();
            return new Calculator(this.appiumDriver);
        }


    }
}
