﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageModel;

// TODO: Add reference to object model
// using PageModel;

namespace Tests
{
    /// <summary>
    /// LoginPageTest test class
    /// </summary>
    [TestClass]
    public class LoginPageTest : BaseSeleniumTest
    {
        /// <summary>
        /// Sample test
        /// </summary>
        [TestMethod]
        public void MagenicLoginPageTest()
        {
            string username = "Ted";
            string password = "123";

            LoginPage loginpage = new LoginPage(this.WebDriver);
            loginpage.OpenPage();
            loginpage.LoginWithValidCredentials(username, password);
         
            HomePage homepage = new HomePage(this.WebDriver);
            homepage.OpenPage();
            Assert.AreEqual("Welcome Home", homepage.ValidateHomeText(), true);
            homepage.clickAboutLink();
            
            AboutPage aboutpage = new AboutPage(this.WebDriver);
            aboutpage.OpenPage();
            Assert.AreEqual("Contact Info", aboutpage.ValidateAboutText(), true);
               
        }
    }
}
