﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for HomePage
    /// </summary>
    public class HomePage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "http://magenicautomation.azurewebsites.net/Static/Training1/HomePage.html";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        /// 
        private static By homeText = By.CssSelector("html body p#WelcomeMessage");

        private static By aboutLink = By.CssSelector("html body table.Nav tbody tr td input#About");


        public string ValidateHomeText()
        {
            return webDriver.WaitForClickableElement(homeText).Text;

        }


        public void clickAboutLink()
        {
            webDriver.WaitForClickableElement(aboutLink).Click();
        }

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomePage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public HomePage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
    }
}
