﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for Login
    /// </summary>
    public class LoginPage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "http://magenicautomation.azurewebsites.net/Static/Training1/loginpage.html";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        private static By usernameInput = By.CssSelector("#UserName");

        private static By passwordInput = By.CssSelector("#Password");

        private static By loginButton = By.CssSelector("#Login");

        private static By loginErrorMessage = By.CssSelector("#LoginError");   

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginPage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public LoginPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
        /*     public void EnterCredentials(string username, string password)
             {
                 this.webDriver.WaitForVisibleElement(username).SendKeys(username);
                 this.webDriver.WaitForVisibleElement(password).SendKeys(password);
             }
             public bool LoginWithInvalidCredentials(string username, string password)
            {
                 this.webDriver.WaitForVisibleElement(username).SendKeys(username);
                 this.webDriver.WaitForVisibleElement(password).SendKeys(password);
                 this.webDriver.FindElement(loginButton).Click();
                 return this.webDriver.WaitUntilVisibleElement(loginErrorMessage);
             } */
        public void EnterCredentials(string username, string password)
        {
            this.webDriver.WaitForVisibleElement(usernameInput).SendKeys(username);
            this.webDriver.WaitForVisibleElement(passwordInput).SendKeys(password);
        }

        public HomePage LoginWithValidCredentials(string userName, string password)
        {
            this.EnterCredentials(userName, password);
            this.webDriver.FindElement(loginButton).Click();

            HomePage newPage = new HomePage(this.webDriver);
            newPage.AssertPageLoaded();
            return newPage;
        }
    }
}
