﻿using Magenic.MaqsFramework.BaseSeleniumTest;
using OpenQA.Selenium;

namespace PageModel
{
    /// <summary>
    /// Page object for AboutPage
    /// </summary>
    public class AboutPage
    {
        /// <summary>
        /// The page url
        /// </summary>
        private const string PageUrl = "http://magenicautomation.azurewebsites.net/Static/Training1/About.html";

        /// <summary>
        /// Sample element 'By' finder
        /// </summary>
        private static By aboutText = By.CssSelector("html body table#AboutTable thead tr td");

        public string ValidateAboutText()
        {
            return webDriver.WaitForClickableElement(aboutText).Text;

        }

        /// <summary>
        /// Selenium Web Driver
        /// </summary>
        private IWebDriver webDriver;

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutPage" /> class.
        /// </summary>
        /// <param name="webDriver">The selenium web driver</param>
        public AboutPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Open the page
        /// </summary>
        public void OpenPage()
        {
            // Open the gmail login page
            this.webDriver.Navigate().GoToUrl(PageUrl);
            this.AssertPageLoaded();
        }

        /// <summary>
        /// Verify we are on the login page
        /// </summary>
        public void AssertPageLoaded()
        {
            //Assert depends on what testing framework is being used
            //Assert.IsTrue(
            //    this.webDriver.Url.Equals(PageUrl, System.StringComparison.CurrentCultureIgnoreCase),
            //    "Expected to be on '{0}', but was on '{1}' instead",
            //    PageUrl,
            //    this.webDriver.Url);
        }
    }
}
